** This is the CASIO 83-FX emulator CPU introspection project **

Created by Rene Lagarde, Feb 2021
CC-BY-4.0

This project is designed to take a look into the heart of the Casio 83-FX calculator, to see how it arrives at a solution.

Inspired by *Matt Parker*, who created a video in 2020 about 'weird' behaviour of this calculator
Youtube: https://www.youtube.com/watch?v=7LKy3lrkTRA

Based on the work by *SopaXorzTaker*, who has provided much of the disassembly and cpu documentation.
*  Github: https://github.com/SopaXorzTaker/fxesplus
*  GitLab: https://gitlab.com/BlueSyncLine

---

**_WARNING:_** Please respect the copyright of CASIO when using this code.
Using the emulator in some ways may violate the licence terms and derived data can be protected by copyright.
Do not publicly distribute copyrighted material without proper consideration.

---

## Getting started

Part one: Dump
1. Install: ClassWiz Emulator Subscription for fx-83GT X_85GT X Ver.2.01
2. Install: Cheat Engine 7.1
3. Open the Emulator
4. Choose your favorite mathematical expression (default: 11^6 / 13)
5. Check the parameters of the LUA dump script, such test name, drive letter etc.
6. Attach Cheat Engine, click memory view and open the Lua script engine
7. Paste the LUA script into the LUA engine and run. 
8. Wait until the script dumps your CPU registers

Part two: Analyse
1. Check the basic stages of the computation using a graph 
2. Read through the annotations and look at the basic structure of the evaluation
3. Zoom in on the individual CPU states
4. Create/remove annotations for a better look inside

---

## Additional material

* https://github.com/SopaXorzTaker/fxesplus

---

## Remaining work / improvements

* Currently, all  annotations have to be directly extracted from  a single cpu state. 
This is convenient because it is simple, yet it may prevent deeper analysis in the future.

* Add a level number to the annotations, we have high level operations and lower level operations.

* Do some work on the intermediate state of a multiplication or division. We know that it does a continued
addition/subtraction, but it must keep track of the intermediate result. If we have that, we can simply show the
intermediate result instead of all the low level annotations. (but see previous point) 

* I have found that when doing a ^2 or a ^3 will use a simple multiplication solution, which takes
About 2000 or 3000 cpu cycles to complete, but a ^4 will take a different route. This method will convert the
base to its logarithm and multiply the logarithm with the power. The result is converted back into
decimal and the result is rounded up if needed. This takes over 40K cpu cycles and the algorithm is given
in the Algorithms folder.