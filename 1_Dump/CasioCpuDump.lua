--[[
This is the CASIO 83-FX emulator CPU dump script

Instructions:
1. Install: ClassWiz Emulator Subscription for fx-83GT X_85GT X Ver.2.01
2. Install: Cheat Engine 7.1
3. Open the Emulator
4. Choose your favorite mathematical expression (default: 11^6 / 13)
5. Check the parameters of the LUA dump script, such test name, drive letter etc.
6. Attach Cheat Engine, click memory view and open the Lua script engine
7. Paste the LUA script into the LUA engine and run.
8. Wait until the script dumps your CPU registers

You can enter an arithmetic expression, and this script will dump all the 
CPU operations and register contents for each CPU cycle.
The math_expr argument is optional, so if you type your own you can omit it.

Inspired by *Matt Parker*, who created a video in 2020
about weird behaviour of this calculator

V0.1 2021-02-04	RLA		Creation, based on exploration in 2020

Created by Rene Lagarde, Feb 2021
CC-BY-4.0

]]--

_pow = "\xc9"; -- raise to power
_cls = "\xd0"; -- close bracket (close the power)
_div = "\xa9"; -- divide
_nul = "\x00"; -- terminate

-- the expression that started it all:
math_expr = "11" .. _pow .. "6" .. _cls .. _div .. "13" .. _nul;
-- the prefix name of the file that is created
test_name = "test";

-- this is what it should be out of the box
exec_fileName = "fx-83GT X_85GT X Emulator.exe";
output_dir = "f:\\";

--------- BREAK POINTS --------------------
-- this is a point to start the evaluation of the input
addr_writeDataMemory = getAddress(exec_fileName) + 0x8C3FC;
-- this point indicates the start of a CPU cycle
addr_execute = getAddress("SimU8Engine.CSimU8core::m_Execute");

-------- MEMORY ADDRESS ------------------
-- find a static address in the dll
addr_static = getAddress("SimU8.dll") + 0x16CE20;
-- this area is 0x100 long, and can be recognized by the text WDTINT
-- it holds the key pointers to the CPU memory segments
-- the decoding of the instructions, and the (complete) CPU state

-- 65K memory segments
-- these pointers can move around, since memory allocation can vary
-- segment 0 (ROM)
addr_segc0 = readInteger(addr_static + 0x30);
-- segments 1,2 (etc??)
addr_segw0 = readInteger(addr_static + 0x4C);
addr_seg1 = readInteger(addr_static + 0x68);

-- input line for your expression
-- you can type it manually on the calculator
-- or write the expression string to this address (null terminated)
addr_inputLine = addr_segw0 + 0xD180;

-------- GLOBAL VARS -----
init_done = 0;
fout = nil;
cpu_counter = 0;

-- end when we reach instruction 2-4c00 (reversed)
end_pc = 0x0D01E;
end_pc = 0x06E60;
-- or on a timeout counter
end_count_max = 200000;
end_count_min = 1000;

-- return a byte string from memory
function readData(addr, size)
	data_table = readBytes(addr, size, 1);
	result = "";
    for i, d in ipairs(data_table) do
		result = result .. string.char(d);
	end;
	return result;
end;

function printHex(nr)
	print(string.format('%05X', nr));
end

-- callback on each simulated cpu cycle
function on_execute()
	if (init_done == 0) then
		cpu_counter = 0;
		fout = io.open(output_dir .. test_name .. "_dump.hex", "wb");
		
		-- i will first take some snapshots, these can be interesting later, but not required
		if false then
            writeRegionToFile(output_dir .. test_name .. "_static.hex", addr_static, 0x100);
            writeRegionToFile(output_dir .. test_name .. "_segc0.hex", addr_segc0, 0x10000);
            writeRegionToFile(output_dir .. test_name .. "_segw0.hex", addr_segw0, 0x14000);
            writeRegionToFile(output_dir .. test_name .. "_seg1.hex", addr_seg1, 0x60000);
        end
		init_done = 1;
		print("dump started")
	end

	-- collect data
    local data = readData(addr_static + 4, 26) .. readData(addr_static + 128, 66);
	fout:write(data);
	
	-- read the program counter (CSR + PC)
	local cpu_pc = readBytes(addr_static + 158, 1) * 0x10000 + readSmallInteger(addr_static + 148);
	--printHex(cpu_pc);
	--printHex(end_pc);
	--print(tostring(cpu_pc == end_pc));

	local end_condition = ((cpu_counter > end_count_min and cpu_pc == end_pc) or (cpu_counter >= end_count_max));
	
	if end_condition then
		if cpu_counter == end_count_max then
			print("end condition timeout");
		else
			print("execution done");
		end
		print(cpu_counter);
		
		debug_removeBreakpoint(addr_execute);
		fout:close();	
	end

	cpu_counter = cpu_counter + 1;
end


function evaluate(dump_name, expression)
    -- set a name for the dump
    if (type(dump_name) == "string") then
        test_name = dump_name;
    end

	-- if you have entered manually, 
	-- you do not need to provide an expression string
	if (type(expression) == "string") then
		-- todo: write to input line
		print("writing expression");
		writeString(addr_inputLine, expression, False);
	end
	
	local function evaluate_callback()
		print("evaluate_callback");

		-- force the evaluation
		writeBytes(EBP-2, 0x40, 0x01);

		-- hand off to the execute callback
		-- make sure it does an init
		init_done = 0;
		debug_setBreakpoint(addr_execute, on_execute);
		debug_removeBreakpoint(addr_writeDataMemory);
		--debug_continueFromBreakpoint(co_run);
	end
	
	-- wait until we hit the breakpoint and hit evaluate
	debug_setBreakpoint(addr_writeDataMemory, evaluate_callback);
end

-- and lets go
evaluate(dump_name, math_expr);
