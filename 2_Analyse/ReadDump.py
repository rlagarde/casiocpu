"""Main file for reading the casio CPU register dump

Developed on Win10, using Python 3.6 (Anaconda)
However, this should run in other platforms as long as you are using Python 3

Created by Rene Lagarde, Feb 2021
CC-BY-4.0

"""


import os
import sys
import time
from binascii import hexlify, unhexlify
import tempfile
import matplotlib.pyplot as plt
import numpy as np

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from BasicTools import *
from Disassemble import *
from CasioStruct import *
from Store import *
from CliMenu import CliMenu


# exception for quit and remove database
class QuitAndDelete(BaseException):
    pass

plot_process = None

annotations = {
    0x192CC: (1, '= Exp10 =', ),
    0x190CC: (1, '= Log10 =', ),
    0x194F6: (1, '= Multiply ='),
    0x1906E: (1, '= Division ='),
    0x20068: (1, '= Fractional ='),
    0x1A8E6: (1, '= Division ='),
    0x1A8EC: (1, '= Multiply by 25200 ='),
    0x1B2FA: (1, '= GCD 25200 ='),
    0x1A324: (2, 'ADD', ),
    0x1A5F6: (2, 'ADD', ),
    0x1A31E: (2, 'ADD', ),
    0x17474: (2, 'ADD', ),
    0x1A350: (2, 'SUB', ),
    0x1A626: (2, 'SUB', ),
    0x1A7FE: (2, 'SUB', ),
    0x1A34A: (2, 'SUB', ),
    0x195B8: (2, 'SUB', ),  # reduced length
    0x1994E: (5, 'STORE8', ),
    0x19A98: (5, 'STORE10', ),
    0x19728: (5, 'STORE10', ),
    0x199D4: (5, 'STORE10', ),
    0x1480A: (4, 'Parse', ),
    0x0AF7E: (4, 'Parse', ),
    0x2035C: (4, 'Parse', ),
    0x13ECC: (4, 'Parse', ),
    0x19D22: (5, 'Form', ),
    # (0x1A3BC, '3, 8),
    # 0x0AF60: 'Parse',
    # 0x1A1D6: 'Exp',
    # 0x19932: 'Exp',
    }


class U8:
    segments = (30, 62)

    def __init__(self):
        self.segments_cumulative = tuple(sum(self.segments[:_i]) for _i in range(len(self.segments) + 1))

        self.hex_data = bytearray()

        self.regs = Regs()
        self.info = Info()

        self.expl = ''
        self.checks = {}

    def copy(self):
        r = U8()
        r.hex_data = self.hex_data
        r.regs = self.regs
        r.info = self.info

        r.intro = self.intro
        return r

    def load_from(self, other):
        self.load_hex(other.hex_data)

    def load_hex(self, hx):
        self.hex_data = hx

        if len(hx) < 1:
            return

        self.info = Info.from_buffer_copy(hx, self.segments_cumulative[0])
        self.regs = Regs.from_buffer_copy(hx, self.segments_cumulative[1])

    def print_hex(self):
        """print all hex data for this time slice"""
        print(' '.join(hexlify(self.hex_data[_x1:_x2]).decode() for _x1, _x2 in
                       zip(self.segments_cumulative,
                           self.segments_cumulative[1:])))

    def get_reg(self, i, size):
        return unpack(FMT[size], self.hex_data[self.segments_cumulative[1] + i:
                                               self.segments_cumulative[1] + i + size])[0]

    def get_operand(self, o):
        if not o:
            return o

        if '[' in o:
            i0, i1 = o.index('['), o.index(']')
            expr = o[:i0], o[i0+1: i1], o[i1+1:]
            val = [self.get_operand(e) for e in expr]
            return val[0] + '[' + val[1] + ']' + val[2]

        if ' + ' in o:
            i0 = o.index(' + ')
            expr = o[: i0], o[i0+3:]
            val = [self.get_operand(e) for e in expr]
            return val[0] + ' + ' + val[1]

        if ' - ' in o:
            i0 = o.index(' - ')
            expr = o[: i0], o[i0+3:]
            val = [self.get_operand(e) for e in expr]
            return val[0] + ' - ' + val[1]

        if o.startswith('R'):
            s_bi = None
            if '.' in o:
                s_by, s_bi = tuple(int(_o) for _o in o[1:].split('.'))
            else:
                s_by = int(o[1:])
            v = self.get_reg(s_by, 1)
            if s_bi is not None:
                v = (v >> s_bi) & 1
            return '0x{:X}'.format(v)

        if o in COND:
            # psw: C Z S OV MIE HC - ELEVEL12
            return self.get_psw()

        r = self.get_operand_ref(o)
        if r:
            v = self.get_reg(r[0], r[1])
            return '0x' + to_hex(v, 2*r[1])

        return o

    def get_operand_ref(self, o):
        if not o:
            return None

        # substitutions
        if o == 'BP':
            o = 'ER12'
        if o == 'FP':
            o = 'ER14'

        if o.startswith('R'):
            s_bi = None
            if '.' in o:
                s_by, s_bi = tuple(int(_o) for _o in o[1:].split('.'))
            else:
                s_by = int(o[1:])
            return s_by, 1
        if o.startswith('ER'):
            return int(o[2:]), 2
        if o.startswith('XR'):
            return int(o[2:]), 4
        if o.startswith('QR'):
            return int(o[2:]), 8
        if o == 'PC':
            return 16, 2
        if o == 'LR':
            return 18, 2
        if o == 'CSR':
            return 24, 2
        if o == 'LCSR':
            return 26, 2
        if o == 'LCSR':
            return 26, 2
        if o == 'PSW' or (o in COND and o != 'AL'):
            return 31, 1
        if o == 'SP':
            return 34, 1
        if o.startswith('EA'):
            return 38, 2
        if o == 'DSR':
            return 40, 1

        return None

    def get_pc(self):
        """get program counter (20 bit)"""
        return self.regs.CSR << 16 | self.regs.PC

    def get_sp(self):
        """get stack pointer"""
        return self.regs.SP

    def get_psw(self):
        """return the program status flags (conditionals)"""
        psw = self.regs.PSW >> 2
        b = '{:06b}'.format(psw)
        return ''.join(c if b == '1' else '_' for c, b in zip('CZSOMH', b))


class Context:
    def __init__(self, source_filename, db_filename, dump_id):
        url = 'sqlite:///' + db_filename
        self.db_url = url
        self.dump_id = dump_id
        self.engine = create_engine(url, echo=False)
        # create the db structure if not already there
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

        self.hex_filename = source_filename
        self.hex_file = open(source_filename, 'rb')

        self.cpu0 = U8()  # previous state
        self.cpu1 = U8()  # current state

        self.chunksize = self.cpu0.segments_cumulative[-1]
        self.statesize = os.path.getsize(source_filename) // self.chunksize

        self.line_nr = 0

        self.menu = CliMenu(
            header="CASIO CPU dump analysis\n",
            options=[(self.show_basic, 'Show basic properties'),
                     (self.show_outline, 'Show Outline Graph'),
                     (self.show_annotations, 'Show Annotation List'),
                     (self.show_details, 'Show Cpu Internal State'),
                     (self.fill_db, 'Update Database'),
                     (self.clear_db, 'Quit and delete database'),])

    def show_basic(self):
        hex_status = 'HEX FILE:\nFilename:  {:s}\nStates:  {:d}'.format(self.hex_filename, self.statesize)

        nr_cpu_states = self.session.query(CpuState).count()
        nr_instructions = self.session.query(Instruction).count()
        db_status = "DATABASE:\nFilename:  {:s}\nInstructions:  {:d}\nCPU states:    {:d}".format(self.db_url,
                                                                                                  nr_instructions,
                                                                                                  nr_cpu_states)

        print('\n'.join((hex_status, db_status, '')))

    def clear_db(self, are_you_sure):
        if not are_you_sure:
            print("returning to menu")
            return
        print("clearing database")
        self.session.close()
        del self.session
        del self.engine
        raise QuitAndDelete("quit program and erase database")

    def fill_db(self, annotation_refresh):
        print("updating database")

        if annotation_refresh:
            for s in self.session.query(CpuState). \
                    filter(CpuState.note is not None). \
                    filter(CpuState.pc not in annotations.keys()):
                self.data_seek(s.line_id)
                self.update_store()

            for s in self.session.query(CpuState).filter(CpuState.pc in annotations.keys()):
                self.data_seek(s.line_id)
                self.update_store()
        else:
            self.data_seek(1)
            while 1:
                self.update_store()
                if not self.data_next():
                    break
        # make sure data is saved
        self.session.commit()

    def show_outline(self, max_level):
        print("showing outline")
        global plot_process
        # opening it in a different process is a bit overkill, but hey
        try:
            from multiprocessing import Process
            plot_process = Process(target=show_plots, args=(self.db_url, self.dump_id, max_level))
            print("opening the plots in a separate process")
            plot_process.start()
            time.sleep(2)
        except ImportError:
            show_plots(self.db_url)

    def show_details(self, line_start, line_count):
        print("showing details")
        while 1:
            self.data_seek(int(line_start))
            for i in range(int(line_count)):
                a_lvl, a = self.create_annotation()
                if a:
                    print(Fore.GREEN + a + Fore.RESET)
                self.print_basics()
                self.update_store()
                if not self.data_next():
                    break

            self.session.commit()
            print("press PageUp and PageDown to scroll")
            choice = getch()
            if choice == b'\xe0':
                choice = getch()
            if choice == b'\x49':
                line_start -= line_count
                if line_start < 1:
                    line_start = 1
            elif choice == b'\x51':
                line_start += line_count
            else:
                break
            clear()

    def data_seek(self, cursor):
        """seek the opened file, fill the current cpu state, and the next cpu state for results"""
        assert cursor > 0, "cannot load cursor == 0, because states before and after are required"
        self.hex_file.seek((cursor-1) * self.chunksize)
        self.line_nr = cursor-2
        self.data_next()
        self.data_next()

    def data_next(self):
        """cycle the cpu state to tne next state"""
        data1 = self.hex_file.read(self.chunksize)
        if len(data1) != self.chunksize:
            return False
        self.load_data(data1)
        return True

    def load_data(self, data):
        """load data into cpu objects"""
        self.cpu0.load_from(self.cpu1)
        self.cpu1.load_hex(data)
        # return and ask for another line

        self.line_nr += 1

    def update_store(self):
        """update sqlite database one state at a time"""
        if self.line_nr <= 1:
            return

        # the info structure reflects the operation between previous and current state
        info = self.cpu1.info
        instr_words = info.I0, info.I1

        pc = self.cpu0.get_pc()
        sp = self.cpu0.get_sp()

        # print(to_hex(pc, 5))
        i_size, operator, operands, mode = disassemble(instr_words)
        attr = {"pc": pc, "size": i_size, "operator": operator}
        if len(operands) > 0:
            attr["operand1"] = operands[0]
        if len(operands) > 1:
            attr["operand2"] = operands[1]

        instr = Instruction(**attr)
        self.session.merge(instr)

        note_lvl, note = self.create_annotation()
        attr = {"line_id": self.line_nr, "pc": pc, "sp": sp, 'note': note, 'note_lvl': note_lvl}
        state = CpuState(**attr)
        self.session.merge(state)

        if note:
            print('{:d} - {:s}'.format(self.line_nr, note))

        if self.line_nr % 1000 == 0:
            self.session.commit()

    def create_annotation(self):
        pc = self.cpu0.get_pc()
        if pc in annotations.keys():
            a = annotations[pc]
            lvl, hint = a[:2]
            args = a[2:]
            a, b = self.cpu0.get_reg(0, 8), self.cpu0.get_reg(8, 8)
            ha = to_hex(a, 16)
            hb = to_hex(b, 16)

            if hint == 'ADD':
                hint = '{:s}: {:s} + {:s}'.format(hint, ha, hb)
            elif hint == 'SUB':
                hint = '{:s}: {:s} - {:s}'.format(hint, ha, hb)
            elif hint == 'STORE8':
                hint = '{:s}: {:s}'.format(hint, ha)
            elif hint == 'STORE10':
                hint = '{:s}: {:s}'.format(hint, (hb + ha)[-20:])
            elif hint == 'Parse':
                hint = '{:s}: {:s}'.format(hint, ha[-2:])

            return lvl, hint
        else:
            return None, None

    def show_annotations(self):
        print("showing annotations")
        for d in self.session.query(CpuState).filter(CpuState.note != None):
            s_line = str(d.line_id)
            print(s_line + ' ' * (10 - len(s_line)) + d.note)

    def print_basics(self):
        if self.line_nr <= 1:
            return
        # the info structure reflects the operation between previous and current state
        info = self.cpu1.info
        instr_words = info.I0, info.I1

        pc = self.cpu0.get_pc()
        sp = self.cpu0.get_sp()

        print_condition = 1

        i_size, operator, operands, mode = disassemble(instr_words)

        line_expl = "{:05d}: {:05X}: ".format(self.line_nr, pc)
        line_expl += ' '*(40 - len(line_expl)) + operator + ' '*(7-len(operator)) + ', '.join(operands)

        desc = ''
        read_area = []
        write_area = []
        if mode == MODE_JUMP:
            write_area.append(self.cpu0.get_operand_ref('PC'))
        if len(operands):
            desc = [self.cpu1.get_operand(operands[0])]
            for i, o in enumerate(operands):
                desc.append(self.cpu0.get_operand(o))
                ref = self.cpu0.get_operand_ref(o)
                if ref:
                    if i == 0 and (mode in (MODE_COMB, MODE_LOAD)):
                        write_area.append(ref)
                    else:
                        read_area.append(ref)

        line_expl += ' '*(100 - len(line_expl)) + colorize_registers_reverse(bytes(self.cpu0.regs), read_area, write_area)

        if print_condition and 1:   # not intro_active:
            # reminder.append(pc_full)
            print(line_expl)
            if desc:
                print(' / '.join(desc))
            sp_change = self.cpu1.regs.SP - self.cpu0.regs.SP
            pc_change = self.cpu1.get_pc() - self.cpu0.get_pc()
            if sp_change:
                print("SP change: {:d}".format(sp_change))
            if pc_change not in (2, 4):
                print("jump")


def show_plots(url, name='test_dump', max_lvl=10):
    """We get a pretty good overview of different phases of the program, by watching the
    program counter (PC) and stack pointer (SP) over time. (line_nr in dump)
    We can generally use this picture as a road-map for what is captured in the dump
    -- PC --
    The will run in 2 byte or 4 byte increments, depending on the instruction.
    It can also jump, anywhere where there is program memory (in general this memory is not available as data,
    and hidden except for a small ROM window)
    When doing repetitions, we see a sawtooth until the computation is finished: /|/|/|/|/|
    There is less of a pattern here, but it can indicate that we jump to a new task.
    -- SP --
    Note that stack grows downward, so low values have a lot on stack.
    When the stack jumps up, this generally means that a computation is finished, and intermediate results
    can be cleared.
    """
    engine = create_engine(url, echo=False)
    session = sessionmaker(bind=engine)()
    db_data = session.query(CpuState)
    data = np.array(tuple((d.line_id, d.pc, d.sp) for d in db_data), dtype=np.int32).transpose()
    fig = plt.figure(0)
    sub1 = plt.subplot(2, 1, 1, title=name)
    sub2 = plt.subplot(2, 1, 2, title="stack", sharex=sub1)
    # matches = np.isin(data[1], intro_addr)
    l = sub1.plot(data[0], data[1])
    # l = sub1.plot(data[0][matches], data[1][matches], 'r+')

    annotations = tuple((d.line_id, d.note, d.note_lvl) for d in session.query(CpuState).filter(CpuState.note != None))
    for line_nr, note, note_lvl in annotations:
        s_line = str(line_nr)
        # print(s_line + ' ' * (10 - len(s_line)) + note)
        i = np.where(data[0] == line_nr)[0]
        if note_lvl and note_lvl <= max_lvl:
            if note_lvl == 1:
                sub1.annotate(note, (data[0, i], data[1, i]+10000), horizontalalignment='left', fontsize='medium')
            else:
                sub1.annotate(note, (data[0, i], data[1, i]), rotation=90, fontsize='small')

    l = sub2.plot(data[0], data[2])
    sub2.set_xlabel("CPU cycles")

    # fig = plt.figure(1)
    # pc = data[1]
    # pc_count = np.bincount(pc)
    # l = plt.plot(np.arange(0, np.amax(pc)+1)[pc_count > 0], np.bincount(pc)[pc_count > 0], '+')
    # plt.title("code repetition")

    session.close()
    plt.show()


def start_default():
    return Context("d:\\dumps\\test_dump.hex", "C:\\Users\\rlagarde\\AppData\\Local\\Temp\\CASIO_test_dump.db", 'test_dump')


if __name__ == "__main__":
    dump_name = sys.argv[1]
    if not os.path.isfile(dump_name):
        dump_name += '_dump.hex'
    if not os.path.isfile(dump_name):
        raise Exception("Could not find file: " + dump_name)

    dump_filename = os.path.abspath(dump_name)
    dump_id = (os.path.split(dump_filename)[1]).split('.')[0]

    temp_dir = tempfile.gettempdir()
    db_filename = os.path.abspath('{:s}/CASIO_{:s}.db'.format(temp_dir, dump_id))
    if os.path.isfile(db_filename):
        print("re-using database: " + db_filename)
    else:
        print("creating database: " + db_filename)

    try:
        c = Context(dump_filename, db_filename, "11^6/13 breakdown")
        c.menu.run()
    except QuitAndDelete:
        os.remove(db_filename)
        print("database file removed")

    if plot_process is not None:
        plot_process.terminate()







