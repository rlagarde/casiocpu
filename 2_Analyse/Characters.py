

# found on the forum, may come in useful when constructing equations

"""

21 : e
22 : π
23 : :
25 : ?
2C : ;
2D : ×10
2E : ,
30 : 0
31 : 1
32 : 2
33 : 3
34 : 4
35 : 5
36 : 6
37 : 7
38 : 8
39 : 9
40 : M
41 : Ans
42 : A
43 : B
44 : C
45 : D
46 : E
47 : F
48 : x
49 : y
4A : Pré-Rép
4C : θ
60 : (
68 : Abs(
69 : Rnd(
6C : sinh(
6D : cosh(
6E : tanh(
6F : sinh⁻¹(
70 : cosh⁻¹(
71 : tanh⁻¹(
72 : e^
73 : 10^
74 : √(
75 : ln(
76 : ³√(
77 : sin(
78 : cos(
79 : tan(
7A : Arcsin(
7B : Arccos(
7C : Arctan(
7D : log(
7E : Pol
7F : Rec
83 : Ent(
84 : EntEx(
87 : RanInt#(
88 : PGCD(
89 : PPCM(
8A : Arond(
A5 : =
A6 : +
A7 : -
A8 : ×
A9 : ÷
AA : ⊢
AD : P
AE : C
C0 : -
C8 : ⌋
C9 : ^(
CA : ×√(
D0 : )
D4 : ⁻¹
D5 : ²
D6 : ³
D7 : %
D8 : !
D9 : °
DA : ʳ
DB : ᵍ
DC : °
DD : E
DE : P
DF : T
E0 : G
E1 : M
E2 : k
E3 : m
E4 : μ
E5 : n
E6 : p
E7 : f
E9 : ▶Simp
F901 : end of line
F902 : end of program
F903 : empty line
F905 ... 00 : Avancer de ...
F906 ... 00 : Tourner de ↺ ...
F907 ... 00 : S'orienter à ...
F908 ... 00 ... 00 : Aller à x=... ; y=...
F909 : Stylo écrit
F90A : Stylo relevé
F90B ... 00 ... 00 : ... → ... (mettre var à)
F90C ... 00 : ? → ... (Demander valeur)
F90D3100 : "Oui"
F90D3200 : "Non"
F90D3300 : "Nombre?"
F90D3400 : "Résultat:"
F90E ... 00 : Afficher résult ...
F90F3100 : Style Flèche
F90F3200 : Style Croix
F910 : Attendre
F911 ... 00 : Répéter ...
F912 : ⤴ (end of Répéter)
F913 ... 00 : Répéter jusqu'à ...
F914 : ⤴ (end of Répéter jusqu'à)
F915 ... 00 : Si ... Alors [... Fin]
F916 : Fin (end of Si/Alors)
F917 ... 00 : Si ... Alors [... Sinon ... Fin]
F918 : Sinon
F919 : Fin (end of Si/Alors/Sinon)
FB01 : <
FB02 : >
FB03 : ≠
FB04 : ≤
FB05 : ≥
FB10 : →M
FB12 : →A
FB13 : →B
FB14 : →C
FB15 : →D
FB16 : →E
FB17 : →F
FB18 : →x
FB19 : →y
FB1A : M+
FB1B : M-
FD18 : Ran#
"""