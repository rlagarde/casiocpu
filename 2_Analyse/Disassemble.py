
"""
Casio U8 disassembly functions

based on the document:
FEZ0317A0-U8-INST-02
nX-U8/100 Core
Instruction Manual
CMOS 8-bit microcontroller
by OKI SEMICONDUCTOR

Rene Lagarde
2021-02-05
CC-BY-4.0
"""

from struct import pack, unpack
from BasicTools import *


COND = ["GE", "LT", "GT", "LE", "GES", "LTS", "GTS", "LES",
        "NE", "EQ", "NV", "OV", "PS", "NS", "AL", "<Unrecognized>"]
# default struct format
FMT = {1: 'B', 2: 'H', 4: 'I', 8: 'Q'}

MODE_NOP = 0
MODE_STORE = 1
MODE_LOAD = 2
MODE_COMB = 3
MODE_COMP = 4
MODE_JUMP = 5

MATCH_LIBRARY = [
    (('iiiiiiii', '11100011'), 'DSR<-', '0x{i}'),
    (('dddd1111', '10010000'), 'DSR<-', 'R{d}'),
    (('10011111', '11111110'), 'DSR<-', 'DSR'),
    (('mmmm0001', '1000nnnn'), 'ADD', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0001nnnn'), 'ADD', 'R{n}', '{i_s}'),
    (('mmm00110', '1111nnn0'), 'ADD', 'ER{n*2}', 'ER{m*2}'),
    (('1iiiiiii', '1110nnn0'), 'ADD', 'ER{n*2}', '{i_s}'),
    (('iiiiiiii', '11100001'), 'ADD', 'SP', '{i_s}'),
    (('mmmm0110', '1000nnnn'), 'ADDC', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0110nnnn'), 'ADDC', 'R{n}', '{i_s}'),
    (('mmmm0010', '1000nnnn'), 'AND', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0010nnnn'), 'AND', 'R{n}', '0x{i}'),
    (('mmmm0111', '1000nnnn'), 'CMP', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0111nnnn'), 'CMP', 'R{n}', '{i_s}'),
    (('mmm00111', '1111nnn0'), 'CMP', 'ER{n*2}', 'ER{m*2}'),
    (('mmmm0101', '1000nnnn'), 'CMPC', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0101nnnn'), 'CMPC', 'R{n}', '{i_s}'),
    (('mmm00101', '1111nnn0'), 'MOV', 'ER{n*2}', 'ER{m*2}'),
    (('0iiiiiii', '1110nnn0'), 'MOV', 'ER{n*2}', '{i_s}'),
    (('mmmm0000', '1000nnnn'), 'MOV', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0000nnnn'), 'MOV', 'R{n}', '{i_s}'),
    (('mmmm0011', '1000nnnn'), 'OR', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0011nnnn'), 'OR', 'R{n}', '0x{i}'),
    (('mmmm0100', '1000nnnn'), 'XOR', 'R{n}', 'R{m}'),
    (('iiiiiiii', '0100nnnn'), 'XOR', 'R{n}', '0x{i}'),
    (('mmmm1000', '1000nnnn'), 'SUB', 'R{n}', 'R{m}'),
    (('mmmm1001', '1000nnnn'), 'SUBC', 'R{n}', 'R{m}'),
    (('mmmm1010', '1000nnnn'), 'SLL', 'R{n}', 'R{m}'),
    (('0www1010', '1001nnnn'), 'SLL', 'R{n}', '{w}'),
    (('mmmm1011', '1000nnnn'), 'SLLC', 'R{n}', 'R{m}'),
    (('0www1011', '1001nnnn'), 'SLLC', 'R{n}', '{w}'),
    (('mmmm1110', '1000nnnn'), 'SRA', 'R{n}', 'R{m}'),
    (('0www1110', '1001nnnn'), 'SRA', 'R{n}', '{w}'),
    (('mmmm1100', '1000nnnn'), 'SRL', 'R{n}', 'R{m}'),
    (('0www1100', '1001nnnn'), 'SRL', 'R{n}', '{w}'),
    (('mmmm1101', '1000nnnn'), 'SRLC', 'R{n}', 'R{m}'),
    (('0www1101', '1001nnnn'), 'SRLC', 'R{n}', '{w}'),
    (('00110010', '1001nnn0'), 'L', 'ER{n*2}', '[EA]'),
    (('01010010', '1001nnn0'), 'L', 'ER{n*2}', '[EA+]'),
    (('mmm00010', '1001nnn0'), 'L', 'ER{n*2}', '[ER{m*2}]'),
    (('00DDDDDD', '1011nnn0'), 'L', 'ER{n*2}', '[BP + 0x{D}]'),
    (('01DDDDDD', '1011nnn0'), 'L', 'ER{n*2}', '[FP + 0x{D}]'),
    (('00110000', '1001nnnn'), 'L', 'R{n}', '[EA]'),
    (('01010000', '1001nnnn'), 'L', 'R{n}', '[EA+]'),
    (('mmm00000', '1001nnnn'), 'L', 'R{n}', '[ER{m*2}]'),
    (('00DDDDDD', '1101nnnn'), 'L', 'R{n}', '[BP + 0x{D}]'),
    (('01DDDDDD', '1101nnnn'), 'L', 'R{n}', '[FP + 0x{D}]'),
    (('00110100', '1001nn00'), 'L', 'XR{n*4}', '[EA]'),
    (('01010100', '1001nn00'), 'L', 'XR{n*4}', '[EA+]'),
    (('00110110', '1001n000'), 'L', 'QR{n*8}', '[EA]'),
    (('01010110', '1001n000'), 'L', 'QR{n*8}', '[EA+]'),
    (('mmm01010', '11110000'), 'L', 'EA', '[ER{m*2}]'),
    (('00110011', '1001nnn0'), 'ST', 'ER{n*2}', '[EA]'),
    (('01010011', '1001nnn0'), 'ST', 'ER{n*2}', '[EA+]'),
    (('mmm00011', '1001nnn0'), 'ST', 'ER{n*2}', '[ER{m*2}]'),
    (('10DDDDDD', '1011nnn0'), 'ST', 'ER{n*2}', '[BP + 0x{D}]'),
    (('11DDDDDD', '1011nnn0'), 'ST', 'ER{n*2}', '[FP + 0x{D}]'),
    (('00110001', '1001nnnn'), 'ST', 'R{n}', '[EA]'),
    (('01010001', '1001nnnn'), 'ST', 'R{n}', '[EA+]'),
    (('mmm00001', '1001nnnn'), 'ST', 'R{n}', '[ER{m*2}]'),
    (('10DDDDDD', '1101nnnn'), 'ST', 'R{n}', '[BP + 0x{D}]'),
    (('11DDDDDD', '1101nnnn'), 'ST', 'R{n}', '[FP + 0x{D}]'),
    (('00110101', '1001nn00'), 'ST', 'XR{n*4}', '[EA]'),
    (('01010101', '1001nn00'), 'ST', 'XR{n*4}', '[EA+]'),
    (('00110111', '1001n000'), 'ST', 'QR{n*8}', '[EA]'),
    (('01010111', '1001n000'), 'ST', 'QR{n*8}', '[EA+]'),
    (('mmmm1111', '10100000'), 'MOV', 'ECSR', 'R{m}'),
    (('00001101', '1010mmm0'), 'MOV', 'ELR', 'ER{m*2}'),
    (('mmmm1100', '10100000'), 'MOV', 'EPSW', 'R{m}'),
    (('00000101', '1010nnn0'), 'MOV', 'ER{n*2}', 'ELR'),
    (('00011010', '1010nnn0'), 'MOV', 'ER{n*2}', 'SP'),
    (('mmmm1011', '10100000'), 'MOV', 'PSW', 'R{m}'),
    (('iiiiiiii', '11101001'), 'MOV', 'PSW', '{i_s}'),
    (('00000111', '1010nnnn'), 'MOV', 'R{n}', 'ECSR'),
    (('00000100', '1010nnnn'), 'MOV', 'R{n}', 'EPSW'),
    (('00000011', '1010nnnn'), 'MOV', 'R{n}', 'PSW'),
    (('mmm01010', '10100001'), 'MOV', 'SP', 'ER{m*2}'),
    (('01011110', '1111nnn0'), 'PUSH', 'ER{n*2}'),
    (('01111110', '1111n000'), 'PUSH', 'QR{n*8}'),
    (('01001110', '1111nnnn'), 'PUSH', 'R{n}'),
    (('01101110', '1111nn00'), 'PUSH', 'XR{n*4}'),
    (('11001110', '1111lep1'), 'PUSH', '{"LR,"*l}{"EPSW,"*e}{"ELR,"*p}EA'),
    (('11001110', '1111le10'), 'PUSH', '{"LR,"*l}{"EPSW,"*e}ELR'),
    (('11001110', '1111l100'), 'PUSH', '{"LR,"*l}EPSW'),
    (('11001110', '11111000'), 'PUSH', 'LR'),
    (('00011110', '1111nnn0'), 'POP', 'ER{n*2}'),
    (('00111110', '1111n000'), 'POP', 'QR{n*8}'),
    (('00001110', '1111nnnn'), 'POP', 'R{n}'),
    (('00101110', '1111nn00'), 'POP', 'XR{n*4}'),
    (('10001110', '1111lep1'), 'POP', '{"LR,"*l}{"PSW,"*e}{"PC,"*p}EA'),
    (('10001110', '1111le10'), 'POP', '{"LR,"*l}{"PSW,"*e}PC'),
    (('10001110', '1111l100'), 'POP', '{"LR,"*l}PSW'),
    (('10001110', '11111000'), 'POP', 'LR'),
    (('mmmm1110', '1010nnnn'), 'MOV', 'CR{n}', 'R{m}'),
    (('00101101', '1111nnn0'), 'MOV', 'CER{n*2}', '[EA]'),
    (('00111101', '1111nnn0'), 'MOV', 'CER{n*2}', '[EA+]'),
    (('00001101', '1111nnnn'), 'MOV', 'CR{n}', '[EA]'),
    (('00011101', '1111nnnn'), 'MOV', 'CR{n}', '[EA+]'),
    (('01001101', '1111nn00'), 'MOV', 'CXR{n*4}', '[EA]'),
    (('01011101', '1111nn00'), 'MOV', 'CXR{n*4}', '[EA+]'),
    (('01101101', '1111n000'), 'MOV', 'CQR{n*8}', '[EA]'),
    (('01111101', '1111n000'), 'MOV', 'CQR{n*8}', '[EA+]'),
    (('mmmm0110', '1010nnnn'), 'MOV', 'R{n}', 'CR{m}'),
    (('10101101', '1111mmm0'), 'MOV', '[EA]', 'CER{m*2}'),
    (('10111101', '1111mmm0'), 'MOV', '[EA+]', 'CER{m*2}'),
    (('10001101', '1111mmmm'), 'MOV', '[EA]', 'CR{m}'),
    (('10011101', '1111mmmm'), 'MOV', '[EA+]', 'CR{m}'),
    (('11001101', '1111mm00'), 'MOV', '[EA]', 'CXR{m*4}'),
    (('11011101', '1111mm00'), 'MOV', '[EA+]', 'CXR{m*4}'),
    (('11101101', '1111m000'), 'MOV', '[EA]', 'CQR{m*8}'),
    (('11111101', '1111m000'), 'MOV', '[EA+]', 'CQR{m*8}'),
    (('00011111', '1000nnnn'), 'DAA', 'R{n}'),
    (('00111111', '1000nnnn'), 'DAS', 'R{n}'),
    (('01011111', '1000nnnn'), 'NEG', 'R{n}'),
    (('0bbb0000', '1010nnnn'), 'SB', 'R{n}.{b}'),
    (('0bbb0010', '1010nnnn'), 'RB', 'R{n}.{b}'),
    (('0bbb0001', '1010nnnn'), 'TB', 'R{n}.{b}'),
    (('00001000', '11101101'), 'EI'),
    (('11110111', '11101011'), 'DI'),
    (('10000000', '11101101'), 'SC'),
    (('01111111', '11101011'), 'RC'),
    (('11001111', '11111110'), 'CPLC'),
    (('rrrrrrrr', '1100cccc'), 'BC', '{cond[c]}', 'PC + {2 + (r_s << 1)}'),  # treating the condition as an operand
    (('nnn01111', '1000mmm1'), 'EXTBW', 'ER{m*2}', 'ER{n*2}'),  # m and n should be equal
    (('00iiiiii', '11100101'), 'SWI', '{i}'),
    (('11111111', '11111111'), 'BRK'),
    (('nnn00010', '11110000'), 'B', 'ER{n*2}'),
    (('nnn00011', '11110000'), 'BL', 'ER{n*2}'),
    (('mmmm0100', '1111nnn0'), 'MUL', 'ER{n*2}', 'R{m}'),
    (('mmmm1001', '1111nnn0'), 'DIV', 'ER{n*2}', 'R{m}'),
    (('00101111', '11111110'), 'INC', '[EA]'),
    (('00111111', '11111110'), 'DEC', '[EA]'),
    (('00011111', '11111110'), 'RT'),
    (('00001111', '11111110'), 'RTI'),
    (('10001111', '11111110'), 'NOP'),
    (('mmm01000', '1010nnn0', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'ER{n*2}', '[ER{m*2} +{E_s<<8|D_s}]'),
    (('00010010', '1001nnn0', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'ER{n*2}', '[0x{E<<8|D}]'),
    (('mmm01000', '1001nnnn', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'R{n}', '[ER{m*2} +{E_s<<8|D_s}]'),
    (('00010000', '1001nnnn', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'R{n}', '[0x{E<<8|D}]'),
    (('mmm01001', '1010nnn0', 'DDDDDDDD', 'EEEEEEEE'), 'ST', 'ER{n*2}', '[ER{m*2} +{E_s<<8|D_s}]'),
    (('00010011', '1001nnn0', 'DDDDDDDD', 'EEEEEEEE'), 'ST', 'ER{n*2}', '[0x{E<<8|D}]'),
    (('mmm01001', '1001nnnn', 'DDDDDDDD', 'EEEEEEEE'), 'ST', 'R{n}', '[ER{m*2} +{E_s<<8|D_s}]'),
    (('00010001', '1001nnnn', 'DDDDDDDD', 'EEEEEEEE'), 'ST', 'R{n}', '[0x{E<<8|D}]'),
    (('mmm01011', '11110000', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'EA', '[ER{m*2} +{E_s<<8|D_s}]'),
    (('00001100', '11110000', 'DDDDDDDD', 'EEEEEEEE'), 'L', 'EA', '0x{E<<8|D}'),
    (('1bbb0000', '10100000', 'DDDDDDDD', 'EEEEEEEE'), 'SB', '0x{E<<8|D}.{b}'),
    (('1bbb0010', '10100000', 'DDDDDDDD', 'EEEEEEEE'), 'RB', '0x{E<<8|D}.{b}'),
    (('1bbb0001', '10100000', 'DDDDDDDD', 'EEEEEEEE'), 'TB', '0x{E<<8|D}.{b}'),
    (('00000000', '1111gggg', 'CCCCCCCC', 'DDDDDDDD'), 'B', '0X{g<<16|D<<8|C}'),  # 5 digit hex addr (0X)
    (('00000001', '1111gggg', 'CCCCCCCC', 'DDDDDDDD'), 'BL', '0X{g<<16|D<<8|C}')]


def do_match(i, p, m):
    """match the binary pattern and create a matching dictionary"""
    new_m = {}
    for _i, _p in zip(i, p):
        if _p in '01':
            if _i != _p:
                return False

    # matching complete, now fill in all variables
    for _i, _p in zip(i, p):
        if _p not in '01':
            try:
                new_m[_p] *= 2
                new_m[_p] += int(_i)
            except KeyError:
                new_m[_p] = int(_i)

    # for each variable, return the integer, a signed (_s) and hex value (_x)
    for k in new_m.keys():
        v = new_m[k]
        n2 = 2**p.count(k)
        assert k not in m.keys(), "value already in pattern"
        m[k] = v
        if v >= n2//2:
            v -= n2
        m[k + '_s'] = v
    return True


def disassemble(instr_words):
    b_instr = pack('HH', *instr_words)
    i_str = ["{:08b}".format(b) for b in b_instr]

    for m in MATCH_LIBRARY:
        pattern, operator = m[0:2]
        # operands, if any
        operands_raw = m[2:]
        operands = []
        match_dict = {}

        # all( zip) construction automatically matches the 2 word / 4 word versions
        if all(do_match(i, b, match_dict) for i, b in zip(i_str, pattern)):

            match_dict.update({'to_hex': to_hex, 'cond': COND, 'to_signed': to_signed})
            for op in operands_raw:
                while '{' in op:
                    x1, x2 = op.index('{'), op.index('}')
                    expr = op[x1 + 1:x2]
                    val = eval(expr, match_dict)
                    # if preceded by 0x, return hex string
                    preceding2 = op[:x1][-2:]
                    if preceding2 in ('0x', '0X'):
                        assert type(val) == int and val >= 0, "only convert zero or positive ints to hex"
                        if preceding2 == '0X':
                            val = to_hex(val, 5)
                        elif val < 256:
                            val = to_hex(val)
                        else:
                            val = to_hex(val, 4)
                    elif preceding2 == ' +':
                        # cut out the sign
                        x1 -= 1
                        if val == 0:
                            val = ''
                        elif val > 0:
                            val = '+ {:d}'.format(val)
                        else:
                            val = '- {:d}'.format(-val)
                    # default: simply return decimal repr
                    if type(val) == int:
                        val = '{:d}'.format(val)

                    op = op[:x1] + str(val) + op[x2 + 1:]
                operands.append(op)

            mode = MODE_COMB
            if operator in ('POP', 'L', 'MOV'):
                mode = MODE_LOAD
            elif operator in ('PUSH', 'ST'):
                mode = MODE_STORE
            elif operator[:3] in ('CMP',):
                mode = MODE_COMP
            elif operator[:1] == 'B':
                mode = MODE_JUMP

            return len(pattern), operator, operands, mode

    raise LookupError("unable to decode instruction: " + str(instr_words))


