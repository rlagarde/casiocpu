
from colorama import Fore, Back, Style


def clear():
    from os import system, name
    # for windows
    if name == 'nt':
        _ = system('cls')

        # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def bits(x, n_max=0):
    """generate bits from an integer"""
    while x or n_max:
        yield x & 1
        x >>= 1
        if n_max > 0:
            n_max -= 1


def to_hex(x, nibbles=None):
    fmt = '{:X}'
    if nibbles:
        fmt = '{:0' + str(nibbles) + 'X}'
    v = fmt.format(x)
    return v


def to_bcd(x):
    """decimal number to BCD number"""
    sx = str(x)
    return int(sx, 16)


def from_bcd(x):
    sx = '{:X}'.format(x)
    return int(sx)


def to_signed(x, n):
    """interpret n-bit number as signed"""
    s = 2**(n-1)
    if x >= s:
        return '-' + str(s+s-x)
    return str(x)


def colorize_registers_reverse(bytestring, reads, writes, reverse=True):
    b = [to_hex(b, 2) for b in bytestring]

    for i, bi in enumerate(b):
        w = any(_st <= i < _st + _si for _st, _si in writes)
        r = any(_st <= i < _st + _si for _st, _si in reads)
        if w:
            b[i] = Fore.RED + bi + Fore.RESET
        elif r:
            b[i] = Fore.CYAN + bi + Fore.RESET

    groups = []
    for i in range(1 + (len(b)-1)//8):
        if reverse:
            groups.append(''.join(b[8*i:8*i+8][::-1]))
        else:
            groups.append(''.join(b[8*i:8*i+8]))
    return ' '.join(groups)


class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()