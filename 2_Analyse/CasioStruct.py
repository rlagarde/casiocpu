"""
Casio simulated CPU structures

Rene Lagarde
2021-02-05
CC-BY-4.0

"""
from ctypes import Structure, c_uint8, c_uint16, c_uint32
from BasicTools import *


class Regs(Structure):
    """CPU status structure"""
    _fields_ = [('R0', c_uint8),
                ('R1', c_uint8),
                ('R2', c_uint8),
                ('R3', c_uint8),
                ('R4', c_uint8),
                ('R5', c_uint8),
                ('R6', c_uint8),
                ('R7', c_uint8),
                ('R8', c_uint8),
                ('R9', c_uint8),
                ('RA', c_uint8),
                ('RB', c_uint8),
                ('BP', c_uint16),   # base pointer
                ('FP', c_uint16),   # frame pointer
                ('PC', c_uint16),   # program counter
                ('LR', c_uint16),   # link register
                ('pad0', c_uint32),
                ('pad1', c_uint16),
                ('CSR', c_uint8),   # code segment
                ('LCSR', c_uint8),  # link segment
                ('pad10', c_uint8),
                ('pad11', c_uint8),
                ('pad12', c_uint8),
                ('PSW', c_uint8),   # program status flags
                ('pad14', c_uint8),
                ('pad15', c_uint8),
                ('pad16', c_uint16),
                ('SP', c_uint16),   # Stack pointer
                ('EA', c_uint16),   # ..
                ('DSR', c_uint8)]   # data segment
    # 44 bytes total, of which 16 'normal' registers


class Info(Structure):
    """emulator instruction decoder data"""
    _fields_ = [('I0', c_uint16),      # instruction first word
                ('I1', c_uint16),      # instruction second word
                ('I_count', c_uint8),  # instruction word count
                ('f0', c_uint8),
                ('f1', c_uint8),
                ('flag', c_uint8),
                ('cmd', c_uint32),
                ('opt', c_uint32),
                ('cms', c_uint16),
                ('cma', c_uint16),
                ('cmb', c_uint16)]


def print_struct(s):
    print('\n'.join("{:s} = {:s}".format(f[0], to_hex(s.__getattribute__(f[0]))) for f in s._fields_))
