from sqlalchemy import Column, Integer, String, ForeignKey, Table, SmallInteger, MetaData
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Instruction(Base):
    __tablename__ = "instruction"
    pc = Column(Integer, primary_key=True)
    size = Column(SmallInteger)  # one or two words
    operator = Column(String)
    operand1 = Column(String)
    operand2 = Column(String)


class CpuState(Base):
    __tablename__ = "cpu_state"
    line_id = Column(Integer, primary_key=True)
    sp = Column(Integer)
    pc = Column(Integer, ForeignKey("instruction.pc"))
    note = Column(String(length=100))
    note_lvl = Column(SmallInteger)

