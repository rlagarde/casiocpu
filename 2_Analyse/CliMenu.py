from BasicTools import clear, getch

import inspect


class CliMenu:
    def __init__(self, header, options):
        self.header = header
        # provide a list of callbacks - description pairs
        self.options = options
        self.running = True

    def run(self):
        self.running = True

        while self.running:
            self.show()

    def show(self):
        print(self.header)
        for i, mi in enumerate(self.options):
            print('{:d} - {:s}'.format(i + 1, mi[1]))
        print("Press ESC to exit")

        # keep the options less than 10, and create a submenu if you need
        choice = getch()
        if choice not in b'123456789':
            self.running = False
            return

        choice_nr = 0
        try:
            choice_nr = int(choice)
        except ValueError:
            pass

        clear()
        if 0 < choice_nr <= len(self.options):
            f = self.options[choice_nr - 1][0]
            arg_spec = inspect.signature(f)
            arguments = []
            for a in arg_spec.parameters.values():
                while 1:
                    try:
                        arguments.append(int(input('{:s}: '.format(str(a)))))
                        break
                    except ValueError:
                        print("enter valid integer")
            f(*arguments)
        else:
            print("invalid choice, try again")

