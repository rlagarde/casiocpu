from math import log10

pi = 3.1415926535897932384626433832795028841971693993751058209749445923


log_table = [0.301029995663981,  # 0
             0.413926851582250,  # 1
             0.432137378264257,  # 2
             0.434077479318640,  # 3
             0.434272768626696,  # 4
             0.434292310445318,
             0.434294264756155,  # 6
             0.434294460188529,
             0.434294479731779,  # 8
             0.434294481686104,
             0.434294481881537,  # 10
             0.434294481901080,
             0.434294481903034,
             0.434294481903230,
             0.434294481903249,
             0.434294481903251,
             0.434294481903251]


def simpler_log10(x):
    """this is the basic function, try this first"""
    p = 1  # partial product
    r = 0  # result
    for i in range(16):
        while 1:
            f = p + p * 0.1 ** i
            if x < f:
                break
            p = f
            r += log_table[i] * 0.1**i

    # note: multiply return value y by y+log(e)(x/p−1) would improve precision
    return r


def to_log10(x):
    """this is what the calculator is doing, it is more accurate, and uses the
    fact that you can shift the digits along on each step
    but it is hard to understand exactly"""
    assert x > 1
    x -= 1
    sequence = []
    q = 1
    for i in range(16):
        n = 0
        while x > q:
            # print("{:f} - {:f}".format(x, q))
            x -= q
            n += 1
            qp = q * 0.1**i
            # print("{:f} + {:f}".format(q, qp))
            q += qp
        sequence.append(n)
        x *= 10

    print((q, x))

    print(sequence)
    r = 0
    for i in range(16):
        r /= 10
        r += sequence[15-i] * log_table[15-i]

    return r


if __name__ == "__main__":
    x = pi

    print(to_log10(x))
    print(simpler_log10(x))
    print(log10(x))




