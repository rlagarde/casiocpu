
from log10 import log_table


def simple_pow10(x):
    """simpler power method, easier to understand"""
    int_power, frac = divmod(x, 1.0)

    r = 1
    for i in range(17):
        l = log_table[i]
        while frac > l:
            frac -= l
            r += r * 0.1**i
        frac *= 10

    return r * 10**int_power


def to_pow10(x):
    """I am not sure why it uses two steps.
    you can in fact simply keep a """
    int_power, frac = divmod(x, 1.0)

    sequence = []

    for i in range(17):
        l = log_table[i]
        n = 0
        while frac > l:
            frac -= l
            n = n + 1
        sequence.append(n)
        frac *= 10

    print(sequence)
    # i have lost some accuracy, we can make it equal to the dump
    # sequence = [3, 2, 0, 6, 2, 7, 5, 5, 5, 7, 4, 6, 8, 4, 3, 4, 9]
    # frac = 3.67711227154931

    q = 1
    for i in range(17):
        # dividing by 10 is something you would avoid on a binary calculation
        # it is very cheap when using BCD
        frac /= 10
        for j in range(sequence[16-i]):
            frac += q
            q += q*0.1**(16-i)

    # i am really not sure what frac is doing in this calculation
    # it is 0.8740909 when i exit
    # it is exactly equal to q - 1 !!
    # if we mirror it back to log10 calculation i think we see something similar

    return q * 10**int_power


if __name__ == "__main__":
    x = 1.98859949077654  # log of pi**4

    print(simple_pow10(x))
    print(to_pow10(x))


